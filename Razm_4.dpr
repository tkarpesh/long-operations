program L_Op;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  S1, S2, R: string;
  A: Char;
const
  Q = 10;

function AddZeroBefore(S1: string): string;
begin
  Result := '0' + S1;
end;

function AddZeroAfter(S1: string): string;
begin
  Result := S1 + '0';
end;

function DeleteZeroBefore(S1: string): string;
var
  I, J, L: Integer;
  B: string;
  Flag: Boolean;
begin
  if S1[1] = '-' then
  begin
    Flag := True;
    Delete(S1, 1, 1);
  end;
  I := 1;
  L := Length(S1) - 1;
  while S1[I] = '0' do
  begin
    for J := 1 to L do
    begin
      B := B + S1[J + 1];
    end;
    L := Length(B);
    S1 := B;
    Delete(B, 1, L);
    L := L - 1;
  end;
  If Flag then
  begin
    S1 := '-' + S1;
  end;
  Result := S1;
end;

function Comp(N1, N2: Integer):Integer;
var
  K: Integer;
begin
  if N1 >= N2 then
  begin
    K := N1;
  end
  else
  begin
    K := N2;
  end;
  Result := K;
end;

function Sum(S1, S2: string):string; //The function of Adding
var
  N1, N2, K, Number, Dop, I: Integer;
  Answ: string;
begin
  N1 := Length(S1);
  N2 := Length(S2);
  K := Comp(N1, N2);
  if N1 > N2 then
  begin
    for I := (N2 + 1) to K do
    begin
      S2 := AddZeroBefore(S2);
    end;
  end;
  if N2 > N1 then
  begin
    for I := N1 + 1 to K do
    begin
      S1 := AddZeroBefore(S1);
    end;
  end;
  Dop := 0;
  For I := K downto 1 do
  begin
    Number := Dop + StrToInt(S1[I]) + StrToInt(S2[I]);
    If Number > (Q - 1) then
    begin
      Dop := Number div Q;
      Number := Number mod Q;
    end
    else
    begin
      Dop := 0;
    end;
    Answ := IntToStr(Number) + Answ;
  end;
  If Dop >= 1 then
  begin
    Answ := IntToStr(Dop) + Answ;
  end;
  Result := Answ;
end;

function Razn(P1, P2: string): String; //The Function of substracting
var
  Flag: Boolean;
  M1, M2, I, Number, K, Dop: Integer;
  Per, Answ: string;
begin
  if P1 = P2 then
  begin
    Result := '0';
  end
  else
  begin
    M1 := Length(P1);
    M2 := Length(P2);
    Flag := True;
    if M1 = M2 then
    begin
      I := 1;
      while (P1[I] = P2[I]) and (M1 >= I) do
      begin
        I := I + 1;
      end;
      If P1[I] < P2[I] then
      begin
        Flag := False;
      end;
    end
    else
    begin
      if M1 < M2 then
      begin
        Flag := False;
        for I := M1 + 1 to M2 do
        begin
          P1 := AddZeroBefore(P1);
        end;
      end
      else
      begin
        for I := (M2 + 1) to M1 do
        begin
          P2 := AddZeroBefore(P2);
        end;
      end;
    end;
    if not Flag then
    begin
      Per := P1;
      P1 := P2;
      P2 := Per;
      K := M2;
    end
    else
    begin
      K := M1;
    end;
    Number := 0;
    for I := K downto 1 do
    begin
      Dop := StrToInt(P1[I]) - StrToInt(P2[I]) - Number;
      If Dop < 0 then
      begin
        Dop := Dop + Q;
        Number := 1;
      end
      else
      begin
        Number := 0;
      end;
      Answ := IntToStr(Dop) + Answ;
    end;
    If not Flag then
    begin
      Answ := '-' + Answ;
    end;
    Result := Answ;
  end;
end;

function Mult(S1, S2: string): string; //The function of multiplication
var
  M1, M2, I, J, Dop, K: Integer;
  Per, B, Answ: string;
  Flag, Znak: Boolean;
begin
  M1 := Length(S1);
  M2 := Length(S2);
  if S1 = S2 then
  begin
    Znak := False;
  end;
  If (M1 = M2) and (Znak) then
  begin
    I := 1;
    While S1[1] = S2[1] do
    begin
      I := I + 1;
    end;

    if S1[I] > S2[I] then
    begin
      Flag := True;
    end
    else
    begin
      Flag := False;
    end;
  end
  else
  begin
    if M1 > M2 then
    begin
      Flag := True;
    end
    else
    begin
      Flag := False;
    end;
  end;
  if not Flag then
  begin
    Per := S1;
    S1 := S2;
    S2 := Per;
    K := M1;
    M1 := M2;
    M2 := K;
  end;
  Per := '';
  for J := M2 downto 1 do
  begin
    B := '';
    for I := M1 downto 1 do
    begin
      Dop := StrToInt(S1[I]) * StrToInt(S2[J]);
      Per := IntToStr(Dop);
      for K := M1 downto (I + 1) do
      begin
        Per := AddZeroAfter(Per);
      end;
      B := Sum(Per, B);
    end;
    for K := M2 downto (J + 1) do
    begin
      B := AddZeroAfter(B);
    end;
    Answ := Sum(Answ, B);
  end;
  Result := Answ;
end;

function Divis(S1, S2: string): string; //The function of division
var
  M1, M2, J, Dop: Integer;
  Per, Answ, B: string;
begin
  if S2 = '0' then
  begin
    Result := 'Do Not Divide by Zero';
  end
  else
  begin
    M1 := Length(S1);
    M2 := Length(S2);
    Dop := 0;
    B := Razn(S1, S2);
    if B <> '0' then
    begin
      B := DeleteZeroBefore(B);
    end;
    Per := '0';
    while (M1 >= M2) and (B[1] <> '-') do
    begin
      S1 := B;
      Answ := Sum(Answ, '1');
      M1 := Length(S1);
      B := Razn(S1, S2);
      if B <> '0' then
      begin
        B := DeleteZeroBefore(B);
      end;
    end;
    if Answ <> '' then
    begin
      Answ := Answ + '  (module = ' + S1 + ')';
    end
    else
    begin
      Answ := Per + '  (module = ' + S1 + ')';
    end;
    Result := Answ;
  end;
end;

begin
  write('Enter the first long number  - ');
  Readln(S1);
  Write('Enter the second long number  - ');
  Readln(S2);
  write('Entet the operation(+, -, *, /) -     ');
  Readln(A);
  Writeln('Result: ');
  case A of
    '+': R := Sum(S1, S2);
    '-':
    begin
      R := Razn(S1, S2);
      if R <> '0' then
      begin
        R := DeleteZeroBefore(R);
      end;
    end;
    '*': R := Mult(S1, S2);
    '/': R := Divis(S1, S2);
  end;

//  R := Sum(S1, S2);

//  R := Razn(S1, S2);
//  if R <> '0' then
//  begin
//    R := DeleteZeroBefore(R);
//  end;

//  R := Mult(S1, S2);

//  R := Divis(S1, S2);
  Writeln(R);

  Readln;
end.
