program L_Op;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  S1, S2, R: string;

const
  Q = 10;

function AddZeroBefore(S1: string): string;
begin
  Result := '0' + S1;
end;

function AddZeroAfter(S1: string): string;
begin
  Result := S1 + '0';
end;

function DeleteZeroBefore(S1: string): string;
var
  I, J, L: Integer;
  B: string;
  Flag: Boolean;
begin
  if S1[1] = '-' then
  begin
    Flag := True;
    Delete(S1, 1, 1);
  end;
  I := 1;
  L := Length(S1) - 1;
  while S1[I] = '0' do
  begin
    for J := 1 to L do
    begin
      B := B + S1[J + 1];
    end;
    L := Length(B);
    S1 := B;
    Delete(B, 1, L);
    L := L - 1;
  end;
  If Flag then
  begin
    S1 := '-' + S1;
  end;
  Result := S1;
end;

function Comp(N1, N2: Integer):Integer;
var
  K: Integer;
begin
  if N1 >= N2 then
  begin
    K := N1;
  end
  else
  begin
    K := N2;
  end;
  Result := K;
end;

function Sum(S1, S2: string):string; //The function of Adding
var
  N1, N2, K, Number, Dop, I: Integer;
  Answ: string;
begin
  N1 := Length(S1);
  N2 := Length(S2);
  K := Comp(N1, N2);
  if N1 > N2 then
  begin
    for I := (N2 + 1) to K do
    begin
      S2 := AddZeroBefore(S2);
    end;
  end;
  if N2 > N1 then
  begin
    for I := N1 + 1 to K do
    begin
      S1 := AddZeroBefore(S1);
    end;
  end;
  Dop := 0;
  For I := K downto 1 do
  begin
    Number := Dop + StrToInt(S1[I]) + StrToInt(S2[I]);
    If Number > (Q - 1) then
    begin
      Dop := Number div Q;
      Number := Number mod Q;
    end
    else
    begin
      Dop := 0;
    end;
    Answ := IntToStr(Number) + Answ;
  end;
  If Dop >= 1 then
  begin
    Answ := IntToStr(Dop) + Answ;
  end;
  Result := Answ;
end;

function Razn(P1, P2: string): String;
var
  Flag: Boolean;
  M1, M2, I, Number, K, Dop: Integer;
  Per, Answ: string;
begin
  M1 := Length(P1);
  M2 := Length(P2);
  Flag := True;
  if M1 = M2 then
  begin
    I := 1;
    while P1[I] = P2[I] do
    begin
      I := I + 1;
    end;
    If P1[I] < P2[I] then
    begin
      Flag := False;
    end;
  end
  else
  begin
    if M1 < M2 then
    begin
      Flag := False;
      for I := M1 + 1 to M2 do
      begin
        P1 := AddZeroBefore(P1);
      end;
    end
    else
    begin
      for I := (M2 + 1) to M1 do
      begin
        P2 := AddZeroBefore(P2);
      end;
    end;
  end;
  if not Flag then
  begin
    Per := P1;
    P1 := P2;
    P2 := Per;
    K := M2;
  end
  else
  begin
    K := M1;
  end;
  Number := 0;
  for I := K downto 1 do
  begin
    Dop := StrToInt(P1[I]) - StrToInt(P2[I]) - Number;
    If Dop < 0 then
    begin
      Dop := Dop + Q;
      Number := 1;
    end
    else
    begin
      Number := 0;
    end;
    Answ := IntToStr(Dop) + Answ;
  end;
  If not Flag then
  begin
    Answ := '-' + Answ;
  end;
  Result := Answ;
end;

//function Mult(S1, S2: string): string;
//var
//  M1, M2, I, Number, J, Dop, K: Integer;
//  Per, Answ: string;
//begin
//  M1 := Length(S1);
//  M2 := Length(S2);
//  Number := 0;
//  for I := M2 downto 1 do
//  begin
//    Number := 0;
//    for J := M1 downto 1 do
//    begin
//      Dop := StrToInt(S2[I]) * StrToInt(S1[J]) + Number;
//      If Dop > Q - 1 then
//      begin
//        Number := Dop div Q;
//        Dop := Dop mod Q;
//      end;
//      Per := IntToStr(Dop) + Per;
//    end;
//  end;
//  Per := IntToStr(Number) + Per;
//  for K := 1 to (I - 1) do
//  begin
//    Per := Per + '0';
//  end;
//  Answ := Sum(Per, Answ);
//  Result := Answ;
//end;
//
//function Divis(S1, S2: string): string;
//var
//  M1, M2, J, Dop: Integer;
//  Per, Answ: string;
//begin
//  M1 := Length(S1);
//  M2 := Length(S2);
//  Dop := 0;
//  while (M1 >= M2) and (Razn(S1, S2)[1] <> '-') do
//  begin
//    Dop := Dop + 1;
//    S1 := Razn(S1, S2);
//    J := 1;
//    while (S1[J] = '0') do
//    begin
//      J := J + 1;
//      Per := Per + S1[J];
//      S1 := Per;
//    end;
//    M1 := Length(S1);
//  end;
//  Answ := IntToStr(Dop) + ' (module ' + S1 + ')';
//  Result := Answ;
//end;
//
begin
  write('Enter the first long number  - ');
  Readln(S1);
  Write('Enter the second long number  - ');
  Readln(S2);
//  R := Sum(S1, S2);
  R := Razn(S1, S2);
  R := DeleteZeroBefore(R);
//  R := Mult(S1, S2);
//  R := Divis(S1, S2);
  Writeln(R);

  Readln;
end.
